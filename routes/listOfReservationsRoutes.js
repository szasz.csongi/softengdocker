import express from 'express';
import * as db from '../db/listOfReservationsDb.js';
import checkFilterReservations from '../middleware/listOfReservationsMw.js';
import { checkIfAdmin, checkJWTToken } from '../middleware/auth.js';

const router = express.Router();

// GET for lidt of reservations
router.get('/reservations', checkJWTToken, checkIfAdmin, async (req, res) => {
    try {
        const reservations = await db.selectAllReservations();
        res.render('list_of_reservations', { reservations });
    } catch (err) {
        res.write('List of reservations GET error', err);
        res.status(500);
        res.end();
    }
});

// Post for showing just one persons reservations
router.post('/searchReservationUsername', checkJWTToken, checkIfAdmin, async (req, res) => {
    const { username } = req.body;
    try {
        const reservations = await db.findReservationsByUsername(username);
        res.render('list_of_reservations', { reservations });
    } catch (err) {
        res.write('Error in searching the reservations', err);
        res.status(500);
        res.end();
    }
});

// Post for showing reservations after filter
router.post('/filterReservations', checkFilterReservations, checkJWTToken, checkIfAdmin, async (req, res) => {
    const data = req.body;

    console.log(data);
    try {
        const reservations = await db.filterReservations(data);
        res.render('list_of_reservations', { reservations });
    } catch (err) {
        res.write('Error in filtering the reservations', err);
        res.status(500);
        res.end();
    }
});

export default router;
