import express from 'express';
// import * as db from '../db/usersDb.js';
import selectAllPersons from '../db/usersDb.js';
import { checkIfAdmin, checkJWTToken } from '../middleware/auth.js';

const router = express.Router();

// GET for list of users page
router.get('/users', checkJWTToken, checkIfAdmin, async (req, res) => {
    try {
        const persons = await selectAllPersons();
        res.render('users', { persons });
    } catch (err) {
        res.write('Users GET error', err);
        res.status(500);
        res.end();
    }
});

export default router;
