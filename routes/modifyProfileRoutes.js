import express from 'express';
import * as db from '../db/authDb.js';
import { checkJWTToken } from '../middleware/auth.js';

const router = express.Router();

// GET for logged in users data
router.get('/modifyProfile', checkJWTToken, async (req, res) => {
    try {
        const person = await db.selectPersonByUsername(res.locals.payload.newUsername);
        res.render('modify_profile', { person });
    } catch (err) {
        res.write('Users GET error', err);
        res.status(500);
        res.end();
    }
});

export default router;
