import express from 'express';
import eformidable from 'express-formidable';
import path from 'path';
import { existsSync, mkdirSync } from 'fs';
import * as db from '../db/sportfieldsDb.js';
import { checkIfAdmin, checkJWTToken } from '../middleware/auth.js';
import checkPhotoUpload from '../middleware/sportfieldsMw.js';

const router = express.Router();

const uploadDir = path.join(process.cwd(), 'uploadDir');
if (!existsSync(uploadDir)) {
    mkdirSync(uploadDir);
}

// GET for the main page AKA the
router.get('/', async (req, res) => {
    try {
        const fields = await db.findAllFields();
        res.render('showfields', { fields });
    } catch (err) {
        res.write('All Sportfields GET error', err);
        res.status(500);
        res.end();
    }
});

// Post for showing just one type of sportfield
router.post('/searchSportFieldType', async (req, res) => {
    const type = req.body.fieldType;
    try {
        const fields = await db.findFieldByType(type);
        res.render('showfields', { fields });
    } catch (err) {
        res.write('Error in searching the Sport Field', err);
        res.status(500);
        res.end();
    }
});

// Post for showing sportfields after filter
router.post('/filterSportField', async (req, res) => {
    const data = req.body;
    if (data.minPrice === '') {
        data.minPrice = 0;
    }

    if (data.maxPrice === '') {
        data.maxPrice = 10000000;
    }

    console.log(data);
    try {
        const fields = await db.filterField(data);
        res.render('showfields', { fields });
    } catch (err) {
        res.write('Error in filtering the Sportfields', err);
        res.status(500);
        res.end();
    }
});

// GET for details of a sportfield
router.get('/fielddetail',  async (req, res) => {
    const id = parseInt(req.query.fieldID, 10);
    try {
        const [detail, images] = await Promise.all([db.selectField(id), db.selectPhoto(id)]);
        res.render('fielddetail', { detail, images });
    } catch (err) {
        res.write('Sportfield detail GET error', err);
        res.status(500);
        res.end();
    }
});

router.use(eformidable({ uploadDir, keepExtensions: true }));
const images = [[]];

// Post after uploading a photo
router.post('/uploadPhoto', checkPhotoUpload, checkJWTToken, checkIfAdmin, async (req, res) => {
    const fileHandler = req.files.photoForSportField;

    const generatedName = path.basename(fileHandler.path);
    if (!images[req.fields.sportFieldId]) {
        images[req.fields.sportFieldId] = [];
    }
    images[req.fields.sportFieldId].push(fileHandler.name);
    for (let i = 0; i < images[req.fields.sportFieldId].length; i += 1) {
        console.log(images[req.fields.sportFieldId][i]);
    }

    const data = {
        fieldID: req.fields.sportFieldId,
        imageName: generatedName,
    };

    try {
        await db.insertPhoto(data);

        res.redirect('/');
    } catch (err) {
        res.write('Error in posting a picture', err);
        res.status(500);
        res.end();
    }
});

export default router;
