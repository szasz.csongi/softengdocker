import express from 'express';
// import * as db from '../db/newSportFieldDb.js';
import insertField from '../db/newSportFieldDb.js';
import checkField from '../middleware/newSportFieldMw.js';
import { checkIfAdmin, checkJWTToken } from '../middleware/auth.js';

const router = express.Router();

// GET for form that inserts a new sportfield
router.get('/new_sportfield', checkJWTToken, checkIfAdmin, (req, res) => {
    res.render('new_sportfield');
});

// Post after inserting a new sportfield
router.post('/createSoprtField', checkField, checkJWTToken, checkIfAdmin, async (req, res) => {
    try {
        await insertField(req.body);

        res.redirect('/');
    } catch (err) {
        res.write('Error in posting new sportfield', err);
        res.status(500);
        res.end();
    }
});

export default router;
