import express from 'express';
import * as db from '../db/sportfieldsDb.js';
import * as reserveDb from '../db/reserveDb.js';
import {
    checkReservation, fillReservation,
} from '../middleware/reserveMw.js';
import { checkJWTToken } from '../middleware/auth.js';

const router = express.Router();

// GET for reservation page
router.get('/reserve', checkJWTToken, async (req, res) => {
    try {
        const fields = await db.findAllFields();
        const isMistake = 0;
        res.render('reserve', { fields, isMistake });
    } catch (err) {
        res.write('Reserve GET error', err);
        res.status(500);
        res.end();
    }
});

// Post after a new reservation
router.post('/reserve', checkJWTToken, checkReservation, fillReservation, async (req, res) => {
    try {
        req.body.personalName = res.locals.payload.newUsername;
        await reserveDb.insertReservation(req.body);
        const fields = await db.findAllFields();
        const isMistake = 2;

        res.render('reserve', { fields, isMistake });
    } catch (err) {
        res.write('Error in posting a new reservation', err);
        res.status(500);
        res.end();
    }
});

export default router;
