import express from 'express';
import * as db from '../db/apiDb.js';
import { checkIfAdmin, checkJWTToken } from '../middleware/auth.js';

const router = express.Router();

// Getting reservation with a certain ID
router.get('/getReservation/:id', async (req, res) => {
    try {
        const reservation  = await db.findReservation(req.params.id);
        res.json(reservation);
    } catch (err) {
        res.status(500).send({ message: err.message });
    }
});

// Deleting a reservation with a certain ID
router.delete('/deleteReservation/:id', checkJWTToken, async (req, res) => {
    try {
        const user = await db.getReservedUsername(req.params.id);
        if (user.personName ===  res.locals.payload.newUsername
            || res.locals.payload.userRole === 1) {
            await db.deleteReservation(req.params.id);
        }
        res.sendStatus(204);
    } catch (err) {
        res.status(500).send({ message: err.message });
    }
});

// Allowing a user to log in
router.get('/updateConfirmed/:id', checkJWTToken, checkIfAdmin, async (req, res) => {
    try {
        await db.updateConfirmedPerson(req.params.id);
        res.sendStatus(200);
    } catch (err) {
        res.status(500).send({ message: err.message });
    }
});

// Blocking a user
router.get('/updateNotConfirmed/:id', checkJWTToken, checkIfAdmin, async (req, res) => {
    try {
        await db.updateNotConfirmedPerson(req.params.id);
        res.sendStatus(200);
    } catch (err) {
        res.status(500).send({ message: err.message });
    }
});

export default router;
