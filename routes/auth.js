import express from 'express';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import * as db from '../db/authDb.js';
import {
    checkJWTToken, checkLogin, checkModify, checkRegistration, checkRegistrationRest,
} from '../middleware/auth.js';
import secret from '../util/config.js';

const router = express.Router();

// GET for registration page
router.get('/registration', (req, res) => {
    res.render('registration');
});

// Post after a new registration
router.post('/newRegistration', checkRegistration, checkRegistrationRest, async (req, res) => {
    const { password } = req.body;
    const { confirmPassword } = req.body;
    if (password !== confirmPassword) {
        return res.status(401).send('Password does not mach');
    }
    const passwordHash = await bcrypt.hash(password, 10);

    const data = {
        registrationFirstName: req.body.registrationFirstName,
        registrationLastName: req.body.registrationLastName,
        registrationUsername: req.body.registrationUsername,
        email: req.body.email,
        age: req.body.age,
        telephone: req.body.telephone,
        password: passwordHash,
    };

    try {
        await db.insertPerson(data);
    } catch (err) {
        res.write('Registration error', err);
        res.status(500);
        res.end();
    }

    return res.redirect('/auth/login');
});

// GET for login page
router.get('/login', (req, res) => {
    res.render('login');
});

// Post after login
router.post('/newLogin', checkLogin, async (req, res) => {
    const newUsername = req.body.loginUsername;
    const newPassword = req.body.password;

    try {
        const stored = await db.selectPersonByUsername(newUsername);
        if (await bcrypt.compare(newPassword, stored.password)) {
            const  { userRole } = stored;
            const token = jwt.sign({ newUsername, userRole }, secret);

            res.cookie('token', token, {
                httpOnly: true,
                sameSite: 'strict',
            });

            return res.redirect('/');
        }
        return res.status(401).send('Incorrect login');
    } catch (err) {
        res.write('error', err);
        res.status(500);
        return res.end();
    }
});

// Modifying users data
router.post('/newModify', checkJWTToken, checkModify, async (req, res) => {
    const oldPerson = await db.selectPersonByUsername(res.locals.payload.newUsername);
    const { personID } = oldPerson;

    const data = {
        modifyFirstName: req.body.modifyFirstName,
        modifyLastName: req.body.modifyLastName,
        email: req.body.email,
        age: req.body.age,
        telephone: req.body.telephone,
    };

    try {
        await db.updatePerson(data, personID);
    } catch (err) {
        res.write('error', err);
        res.status(500);
        res.end();
    }

    res.redirect('/modifyProfile/modifyProfile');
});

// Logging out
router.get('/logout', (req, res) => {
    res.clearCookie('token');
    res.redirect('/');
});

export default router;
