import path from 'path';
import express from 'express';
import morgan from 'morgan';
import cookieParser from 'cookie-parser';

import requestRoutes from './routes/view.js';
import apiRoutes from './routes/api.js';
import authRoutes from './routes/auth.js';
import newSportFieldRoutes from './routes/newSportFieldRoutes.js';
import reserveRoutes from './routes/reserveRoutes.js';
import usersRoutes from './routes/usersRoutes.js';
import listOfReservationsRoutes from './routes/listOfReservationsRoutes.js';
import modifyProfileRoutes from './routes/modifyProfileRoutes.js';

import { decodeJWTToken } from './middleware/auth.js';

// making the aplication
const app = express();
const staticDir = path.join(process.cwd(), 'static');
const uploadDir = path.join(process.cwd(), 'uploadDir');

app.use(morgan('tiny'));
app.use(cookieParser());
app.use(express.static(staticDir));
app.use(express.static(uploadDir));
app.use(express.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
app.set('views', path.join(process.cwd(), 'views'));

app.use(decodeJWTToken);

app.use('/auth', authRoutes);
app.use('/api', apiRoutes);
app.use('/newSportField', newSportFieldRoutes);
app.use('/reserve', reserveRoutes);
app.use('/users', usersRoutes);
app.use('/listOfReservations', listOfReservationsRoutes);
app.use('/modifyProfile', modifyProfileRoutes);
app.use(requestRoutes);

app.listen(8080, () => {
    console.log('Server listening on http://localhost:8080/');
});
