// Deleting a reservation
async function deleteReservation(event, reservationID) {
    const response = await fetch(`/api/deleteReservation/${reservationID}`, {
        method: 'DELETE',
    });

    if (response.status === 204) {
        const parent = event.target.parentNode;
        parent.remove();
    } else {
        const body = await response.json();
        const printError = document.getElementById(`reservationID${reservationID}`);
        printError.innerHTML = `${printError.innerHTML} ${body.message}`;
    }
}
async function loadReservation(fieldID, loggedInUser, role) {
    const response = await fetch(`/api/getReservation/${fieldID}`);
    const body = await response.json();

    let i = 0;
    const idShowReservations = document.getElementById(`showReservations${fieldID}`);
    if (response.status === 200) {
        if (body.length === 0) {
            idShowReservations.innerHTML = 'No reservations';
        } else {
            idShowReservations.innerHTML = 'Reservations: <br><br>';
            for (i = 0; i < body.length; i += 1) {
                idShowReservations.innerHTML = `${idShowReservations.innerHTML} <div id="reservationID${body[i].reservationID}">`;
                idShowReservations.innerHTML = `${idShowReservations.innerHTML} </div>`;
                const reservationID = document.getElementById(`reservationID${body[i].reservationID}`);

                if (loggedInUser === body[i].personName || role === '1') {
                    reservationID.innerHTML = `${reservationID.innerHTML} <b>Name:</b> ${body[i].personName} <br>`;
                }
                reservationID.innerHTML = `${reservationID.innerHTML} <b>From:</b> ${body[i].startDate} <br>`;
                reservationID.innerHTML = `${reservationID.innerHTML} <b>Till:</b> ${body[i].endDate} <br>`;
                if (loggedInUser === body[i].personName || role === '1') {
                    reservationID.innerHTML = `${reservationID.innerHTML} <input class='delete-button' onclick="deleteReservation(event, ${body[i].reservationID})" type="button" value="Delete">`;
                }
                reservationID.innerHTML = `${reservationID.innerHTML} <br><br><br>`;
            }
        }
    } else {
        idShowReservations.innerHTML = body.message;
    }
}

const eslintError = 1;
if (eslintError === 0) {
    loadReservation(0);
    deleteReservation(0);
}
