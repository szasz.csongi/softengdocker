function checkEmail(str){
    if( !document.forms.formId.email.validity.valid ) {
        showEmailError();
    } else {
        hideEmailError();
    }
}

function checkUrl(str){
    if(!document.forms.formId.webpage.validity.valid){
        showUrlError();
    }
    else{
        hideUrlError();
    }
}

function checkFirstName(str){
    if(!document.forms.formId.firstname.validity.valid){
        showFirstNameError();
    }
    else{
        hideFirstNameError();
    }
}

function checkLastName(str){
    if(!document.forms.formId.lastname.validity.valid){
        showLastNameError();
    }
    else{
        hideLastNameError();
    }
}

function hideSubmit() {
    idSubmitButton = document.getElementById('id-submit-button');
    idSubmitButton.style.display = 'none';
}

function showSubmit() {
    idSubmitButton = document.getElementById('id-submit-button');
    idSubmitButton.style.display = 'block';
}




function showFirstNameError() {
    idFirstNameError = document.getElementById('id-first-name-error');
    idFirstNameError.style.display = 'block';
    hideSubmit();
}

function hideFirstNameError() {
    idFirstNameError = document.getElementById('id-first-name-error');
    idFirstNameError.style.display = 'none';
    if(checkAllValidation()){
        showSubmit();
    }
}

function showLastNameError() {
    idLastNameError = document.getElementById('id-last-name-error');
    idLastNameError.style.display = 'block';
    hideSubmit();
}

function hideLastNameError() {
    idLastNameError = document.getElementById('id-last-name-error');
    idLastNameError.style.display = 'none';
    if(checkAllValidation()){
        showSubmit();
    }
}


function showEmailError() {
    idEmailError = document.getElementById('id-email-error');
    idEmailError.style.display = 'block';
    hideSubmit();
}

function hideEmailError() {
    idEmailError = document.getElementById('id-email-error');
    idEmailError.style.display = 'none';
    if(checkAllValidation()){
        showSubmit();
    }
}

function showUrlError(){
    idUrlError = document.getElementById('id-url-error')
    idUrlError.style.display = 'block';
    hideSubmit();
}

function hideUrlError() {
    idUrlError = document.getElementById('id-url-error');
    idUrlError.style.display = 'none';
    if(checkAllValidation()){
        showSubmit();
    }
}

let valid;

function checkAllValidation(){
    return  (document.forms.formId.firstname.validity.valid &&
            document.forms.formId.lastname.validity.valid &&
            document.forms.formId.date.validity.valid &&
            document.forms.formId.email.validity.valid &&
            valid &&
            document.forms.formId.webpage.validity.valid);
}

let lastModified = document.lastModified;
document.getElementById("footer").innerHTML = lastModified;


function checkPassword(str){
    idShowPassword = document.getElementById("id-show-password");
    if(str === ""){
        idShowPassword.style.display = "none";
    }
    else{
        idShowPassword.style.display = "block";
        idShowPassword.innerHTML = str;
    }
    const re1=/^(?=.*[a-zA-Z])(?=.*[0-9])/;
    const re2=/.{8,}/;
    const re3=/^.{0,10}$/;
    if(!re1.test(str)){
        showNumberPasswordError();
        valid=0;
        return false;
    }
    if(!re2.test(str)){
        showAtLeastEightPasswordError();
        valid=0;
        return false;
    }
    if(!re3.test(str)){
        showMaxTenPasswordError();
        valid=0;
        return false;
    }
    valid=1;
    hidePasswordError();
    return true;
}

function validPassword(str){
    const re1=/^(?=.*[a-zA-Z])(?=.*[0-9])/;
    const re2=/.{8,}/;
    const re3=/^.{0,10}$/;
    if(!re1.test(str)){
        //showNumberPasswordError();
        return false;
    }
    if(!re2.test(str)){
        //showAtLeastEightPasswordError();
        return false;
    }
    if(!re3.test(str)){
        //showMaxTenPasswordError();
        return false;
    }
    //hidePasswordError();
    return true;
}

function showNumberPasswordError(){
    idPasswordError = document.getElementById('id-password-error');
    idPasswordError.style.display = 'block';
    idPasswordError.innerHTML = "Error, password must contain at least one letter and number";
    hideSubmit();
}

function showAtLeastEightPasswordError(){
    idPasswordError = document.getElementById('id-password-error');
    idPasswordError.style.display = 'block';
    idPasswordError.innerHTML = "Error, password must have at least 8 characters";
    hideSubmit();
}

function showMaxTenPasswordError(){
    idPasswordError = document.getElementById('id-password-error');
    idPasswordError.style.display = 'block';
    idPasswordError.innerHTML = "Error, password can not have more than 10 characters";
    hideSubmit();
}

function hidePasswordError(){
    idPasswordError = document.getElementById('id-password-error');
    idPasswordError.style.display = 'none';
    if(checkAllValidation()){
        showSubmit();
    }
}