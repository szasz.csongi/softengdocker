function getIndicesOf(searchStr, str) {    //Returns the posisions of asubstring in a text
    let searchStrLen = searchStr.length;
    if (searchStrLen == 0) {
        return [];
    }
    let startIndex = 0, index, indices = [];
    
    while ((index = str.indexOf(searchStr, startIndex)) > -1) {
        indices.push(index);
        startIndex = index + 1;
    }
    return indices;
}


function bubble(poz,length){
    let lepes = 0;
    let csere = 0;
    let i = 0;
    let n = poz.length;
    do{
        csere=0;
        for(i=0;i<n-1-lepes;i++){
            if(poz[i]>poz[i+1]){
                let seged = poz[i];
                poz[i] = poz[i+1];
                poz[i+1] = seged;
                seged = length[i];
                length[i] = length[i+1];
                length[i+1] = seged;
                csere = 1
            }
        }
        lepes++;
    }while(csere == 1);
}

function getItRed(searchedWords,text){              //Writes the text, and highlights the searched words red in it
    let words = searchedWords.split(" ");
    let i = 0;
    let j = 0;
    let k = 0;
    let poz = [];
    let length = [];
    for ( i=0 ; i < words.length ; i++ ){
        let word = words[i];
        let indexes = getIndicesOf(word,text);
        
        for(j = 0; j < indexes.length; j++){
            let index = indexes[j];
            poz[k] = index;
            length[k] = word.length;
            k = k+1;
        }
    }
    bubble(poz,length);
    let n = text.length;
    let m = poz.length;
    j = 0;
    document.getElementById('result').innerHTML = "";
    for(i = 0; i < text.length; i++){
        if(poz[j] == i){
            k = 0;
            for(k = 0;k < length[j]; k++){
                document.getElementById('result').innerHTML = `${document.getElementById('result').innerHTML}<span style=\"color: red;font-weight: bold;\">${text[i]}</span>`;
                i++;
            }
            i--;
            j++;
        }
        else{
            if(poz[j] < i){
                for(k = i-poz[j]; k < length[j]; k++){
                    document.getElementById('result').innerHTML = `${document.getElementById('result').innerHTML}<span style=\"color: red;font-weight: bold;\">${text[i]}</span>`;
                    i++; 
                }
                i--;
                j++;
            }
            else{
                document.getElementById('result').innerHTML = `${document.getElementById('result').innerHTML}${text[i]}`;
            }
        }
    }
    
}

function scan(){
    let searchedWords = document.getElementById("searched-words").value;
    let text = document.getElementById("text").value;
    getItRed(searchedWords,text);
}