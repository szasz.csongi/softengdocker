// Allowing a user to log in
async function confirmPerson(event, personID) {
    const update = await fetch(`/api/updateConfirmed/${personID}`);

    if (update.status === 200) {
        event.target.remove();
        const changeButton = document.getElementById(`personID${personID}`);
        changeButton.innerHTML = `${changeButton.innerHTML} <input class='btn btn-danger' onclick="blockPerson(event, ${personID})" type='button' value='Block'> `;
    } else {
        const body = await update.json();
        const printError = document.getElementById(`personID=${personID}`);
        printError.innerHTML = `${printError.innerHTML} ${body.message}`;
    }
}

// Blockin a person
async function blockPerson(event, personID) {
    const update = await fetch(`/api/updateNotConfirmed/${personID}`);

    if (update.status === 200) {
        event.target.remove();
        const changeButton = document.getElementById(`personID${personID}`);
        changeButton.innerHTML = `${changeButton.innerHTML} <input class='btn btn-success' onclick="confirmPerson(event, ${personID})" type='button' value='Confirm'> `;
    } else {
        const body = await update.json();
        const printError = document.getElementById(`personID=${personID}`);
        printError.innerHTML = `${printError.innerHTML} ${body.message}`;
    }
}

const eslintError = 1;
if (eslintError === 0) {
    confirmPerson(0);
    blockPerson(0);
}
