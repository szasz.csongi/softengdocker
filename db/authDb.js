import { callQuery } from './connection.js';

// Getting information of a person from database
export function selectPersonByUsername(username) {
    const queryStr = 'SELECT * FROM persons WHERE persons.username = ?';
    return callQuery(queryStr, [username]).then((person) => person[0]);
}

// Inserting a new user into database after registration
export function insertPerson(data) {
    const queryStr = `INSERT INTO Persons
    VALUES (default, ?, ?, ?, ?, ?, ?, ?, 0, 0)`;
    return callQuery(queryStr, [data.registrationFirstName, data.registrationLastName,
        data.registrationUsername, data.email, data.age, data.telephone, data.password]);
}

// Modifyin the data of an user
export function updatePerson(data, personID) {
    const queryStr = `UPDATE Persons
    SET Persons.firstName = ?, Persons.lastName = ?,
    Persons.email = ?, Persons.age = ?, 
    Persons.telephone = ?
    WHERE Persons.personID = ?`;
    return callQuery(queryStr, [data.modifyFirstName, data.modifyLastName,
        data.email, data.age, data.telephone, personID]);
}

// Checking if an username is taken or not
export function checkIfExistsPerson(username) {
    const queryStr = `SELECT COUNT(*) as Counter
    FROM persons
    WHERE persons.username = ?`;
    return callQuery(queryStr, [username]).then((persons) => persons[0]);
}
