import { callQuery } from './connection.js';

// Getting all reservations from database
export function selectAllReservations() {
    return callQuery('SELECT * FROM Reservation ORDER BY reservation.fieldID');
}

// Getting reservations from a certain owner
export function findReservationsByUsername(data) {
    const queryStr = `SELECT * FROM Reservation
    WHERE Reservation.personName LIKE ?
    ORDER BY reservation.fieldID`;
    return callQuery(queryStr, [`%${data}%`]);
}

/// /Getting reservations that meets the filter requirements from database
export function filterReservations(data) {
    const queryStr = `SELECT * FROM Reservation
    WHERE Reservation.personName LIKE ?
    AND Reservation.fieldID LIKE ?
    AND Reservation.startDate > ?
    AND Reservation.endDate < ?
    ORDER BY reservation.fieldID`;
    return callQuery(queryStr, [`%${data.username}%`, `%${data.fieldID}%`, data.minDate, data.maxDate]);
}
