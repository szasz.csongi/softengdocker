import mysql from 'mysql';
import util from 'util';

// Creating connection pool
const connectionPool = mysql.createPool({
    connectionLimit: 10,
    database: 'webprog',
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: 'Csongi123csongi',
});

export const callQuery = util.promisify(connectionPool.query).bind(connectionPool);

// Creating the databases
function createField() {
    return callQuery(`CREATE TABLE IF NOT EXISTS Fields (
        fieldID INT PRIMARY KEY AUTO_INCREMENT,
        fieldtype VARCHAR(255),
        price INT,
        adress VARCHAR(255),
        description TEXT
    )`);
}

function createImageDatabase() {
    return callQuery(`CREATE TABLE IF NOT EXISTS Images (
        imageID INT PRIMARY KEY AUTO_INCREMENT,
        fieldID INT,
        imageName VARCHAR(255)
    )`);
}

function createPersonsDatabase() {
    return callQuery(`CREATE TABLE IF NOT EXISTS Persons (
        personID INT PRIMARY KEY AUTO_INCREMENT,
        firstName VARCHAR(255),
        lastName VARCHAR(255),
        username VARCHAR(255),
        email VARCHAR(255),
        age INT,
        telephone VARCHAR(255),
        password VARCHAR(255),
        userRole INT,
        confirmed INT
    )`);
}

export function createReservation() {
    return callQuery(`CREATE TABLE IF NOT EXISTS Reservation (
        reservationID INT PRIMARY KEY AUTO_INCREMENT,
        personName VARCHAR(255),
        fieldID INT,
        startDate TIMESTAMP,
        endDate TIMESTAMP
    )`);
}

(async () => {
    try {
        await createField();
        await createImageDatabase();
        await createPersonsDatabase();
        await createReservation();
    } catch (err) {
        console.error(err);
    }
})();
