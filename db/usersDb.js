import { callQuery } from './connection.js';

// Getting the users from database
export default function selectAllPersons() {
    return callQuery('SELECT * FROM Persons');
}
