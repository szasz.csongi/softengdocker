import { callQuery } from './connection.js';

// Inserting new sportfield into database
export default function insertField(data) {
    const queryStr = `INSERT INTO Fields
    VALUES (default, ?, ?, ?, ?)`;
    return callQuery(queryStr, [data.fieldtype, data.price, data.adress, data.description]);
}
