import { callQuery } from './connection.js';

// Getting all fields from database
export function findAllFields() {
    return callQuery('SELECT * FROM Fields');
}

// Getting just a certain type of sportfield
export function findFieldByType(data) {
    const queryStr = `SELECT * FROM Fields
    WHERE Fields.fieldtype LIKE ?`;
    return callQuery(queryStr, [`%${data}%`]);
}

// Getting fields that meets the filter requirements from database
export function filterField(data) {
    const queryStr = `SELECT * FROM Fields
    WHERE Fields.fieldtype LIKE ?
    AND Fields.price > ?
    AND Fields.price < ?
    AND Fields.adress LIKE ?
    AND Fields.description LIKE ?`;
    return callQuery(queryStr, [`%${data.fieldtype}%`, data.minPrice, data.maxPrice, `%${data.adress}%`, `%${data.description}%`]);
}

// Getting field with a certain ID from database
export function selectField(id) {
    const queryStr = 'SELECT * FROM Fields WHERE Fields.fieldID = ?';
    return callQuery(queryStr, [id]).then((fields) => fields[0]);
}

// Inserting photo to database
export function insertPhoto(data) {
    const queryStr = `INSERT INTO Images 
    VALUES (default, ?, ?)`;
    return callQuery(queryStr, [data.fieldID, data.imageName]);
}

// Getting the photo name from database
export function selectPhoto(id) {
    const queryStr = 'SELECT imageName FROM images WHERE images.fieldID = ?';
    return callQuery(queryStr, [id]);
}
