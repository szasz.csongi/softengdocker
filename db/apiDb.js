import { callQuery } from './connection.js';

// Getting reservations of a sportfield with a certain ID from database
export function findReservation(id) {
    const queryStr = 'SELECT * FROM reservation WHERE reservation.fieldID = ?';
    return callQuery(queryStr, [id]);
}

// Deleting from database a reservation
export function deleteReservation(id) {
    const queryStr = 'DELETE FROM reservation WHERE reservation.reservationID = ?';
    return callQuery(queryStr, [id]);
}

// Getting the owner of a reservation from database
export function getReservedUsername(id) {
    const queryStr = 'SELECT reservation.personName FROM reservation WHERE reservation.reservationID = ?';
    return callQuery(queryStr, [id]).then((reservations) => reservations[0]);
}

// Allowing a user to be able to log in
export function updateConfirmedPerson(personID) {
    const queryStr = `UPDATE persons 
    SET persons.confirmed = 1 
    WHERE persons.personID = ?`;
    return callQuery(queryStr, [personID]);
}

// Blocking a user
export function updateNotConfirmedPerson(personID) {
    const queryStr = `UPDATE persons 
    SET persons.confirmed = 0 
    WHERE persons.personID = ?`;
    return callQuery(queryStr, [personID]);
}
