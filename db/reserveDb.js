import { callQuery } from './connection.js';

// Inserting a new reservation into database
export function insertReservation(data) {
    const queryStr = `INSERT INTO Reservation 
    VALUES (default, ?, ?, ?, ?)`;
    return callQuery(queryStr, [data.personalName, data.fieldID, data.from, data.till]);
}

// Checking how many times is a certain sportfield booked on a certain time intervall
export function checkBooked(data) {
    const queryStr = `SELECT COUNT(*) as Counter
    FROM reservation
    WHERE reservation.fieldID = ? 
    AND reservation.startDate <= ? 
    AND reservation.endDate >= ?`;
    return callQuery(queryStr,
        [data.currID, data.currTill, data.currFrom]).then((fields) => fields[0]);
}
