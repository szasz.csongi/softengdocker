/// /Checking if new sportfield form was completed correctly
export default function checkField(req, res, next) {
    if (!req.body.fieldtype) {
        return res.status(400).send('The fieldtype is empty');
    }

    const price = parseFloat(req.body.price);
    if (price < 5 || price > 300
    || Number.isNaN(price)) {
        return res.status(400).send('The price is invalid or it is not a number');
    }

    if (!req.body.adress) {
        return res.status(400).send('Adress is empty');
    }

    if (!req.body.description) {
        return res.status(400).send('Description is empty');
    }

    return next();
}
