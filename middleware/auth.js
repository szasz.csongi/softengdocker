import jwt from 'jsonwebtoken';
import * as db from '../db/authDb.js';
import secret from '../util/config.js';

export function decodeJWTToken(req, res, next) {
    res.locals.payload = {};
    if (req.cookies.token) {
        try {
            res.locals.payload = jwt.verify(req.cookies.token, secret);
        } catch (e) {
            res.clearCookie('token');
        }
    }
    next();
}

// Checking if user is logged in
export function checkJWTToken(req, res, next) {
    if (!(res.locals.payload && Object.keys(res.locals.payload).length === 0
    && res.locals.payload.constructor === Object
    )) {
        console.log(res.locals.payload);
        return next();
    }
    return res.redirect('/auth/login');
}

// Checking if login form was completed correctly
export async function checkLogin(req, res, next) {
    if (!req.body.loginUsername) {
        return res.status(400).send('Username is empty');
    }

    if (!req.body.password) {
        return res.status(400).send('Password is empty');
    }

    try {
        const [mistake, person] = await Promise.all([db.checkIfExistsPerson(req.body.loginUsername),
            db.selectPersonByUsername(req.body.loginUsername)]);
        if (mistake.Counter === 0) {
            return res.status(401).send('There is no such username, you should registrate');
        }
        if (person.confirmed === 0) {
            return res.status(403).send('Your account is not confirmed by an admin');
        }
    } catch (err) {
        res.write('Login error: ', err);
        res.status(500);
        res.end();
    }

    return next();
}

// Checking if registration form was completed correctly
export async function checkRegistration(req, res, next) {
    if (!req.body.registrationFirstName) {
        return res.status(400).send('First name is empty');
    }

    if (!req.body.registrationLastName) {
        return res.status(400).send('Last name is empty');
    }

    if (!req.body.registrationUsername) {
        return res.status(400).send('Username is empty');
    }

    if (!req.body.password) {
        return res.status(400).send('Password is empty');
    }

    try {
        const mistake = await db.checkIfExistsPerson(req.body.registrationUsername);
        if (mistake.Counter !== 0) {
            return res.status(401).send('There is allready a user using this username!');
        }
    } catch (err) {
        res.write('Registration error: ', err);
        res.status(500);
        res.end();
    }

    return next();
}

// Checking if registration form was completed correctly 2.0
// (ESLINT did not let me to put in the same function)
export function checkRegistrationRest(req, res, next) {
    if (!req.body.email) {
        return res.status(400).send('Email is empty');
    }

    if (!req.body.age || Number.isNaN(req.body.age)) {
        return res.status(400).send('Age is empty or it is not a number');
    }

    if (!req.body.telephone) {
        return res.status(400).send('Telephone is empty');
    }

    return next();
}

// Checking if user modify form was completed correctly
export function checkModify(req, res, next) {
    if (!req.body.modifyFirstName) {
        return res.status(400).send('First name is empty');
    }

    if (!req.body.modifyLastName) {
        return res.status(400).send('Last name is empty');
    }

    if (!req.body.email) {
        return res.status(400).send('Email is empty');
    }

    if (!req.body.age || Number.isNaN(req.body.age)) {
        return res.status(400).send('Age is empty or it is not a number');
    }

    if (!req.body.telephone) {
        return res.status(400).send('Telephone is empty');
    }

    return next();
}

// Checking if person is admin or not
export async function checkIfAdmin(req, res, next) {
    const person = await db.selectPersonByUsername(res.locals.payload.newUsername);
    if (person.userRole === 0) {
        return res.status(403).send('Your are not admin');
    }

    return next();
}
