import * as db from '../db/sportfieldsDb.js';
import * as reserveDb from '../db/reserveDb.js';

// Checking if reservation form was completed correctly
export async function checkReservation(req, res, next) {
    const from = new Date(req.body.from);
    const till = new Date(req.body.till);
    const fields = await db.findAllFields();
    const isMistake = 1;
    if (!req.body.personalName) {
        return res.render('reserve', { fields, isMistake });
    }

    // Checks if from is Nan or not, because Nan is never equal to itself
    if (!req.body.from || !(from.getTime() === from.getTime() + 1 - 1)) {
        return res.render('reserve', { fields, isMistake });
    }

    if (!req.body.till || !(till.getTime() === till.getTime() + 1 - 1)) {
        return res.render('reserve', { fields, isMistake });
    }

    if (from.getTime() > till.getTime()) {
        return res.render('reserve', { fields, isMistake });
    }

    return next();
}

// Checking if the sportfield is free at a certain date intervall
export async function fillReservation(req, res, next) {
    const from = new Date(req.body.from);
    const till = new Date(req.body.till);
    const currID = req.body.fieldID;
    const currFrom = from;
    const currTill = till;

    const reservation = {
        currID,
        currFrom,
        currTill,
    };

    try {
        const [fields, mistake] = await Promise.all([db.findAllFields(),
            reserveDb.checkBooked(reservation)]);
        if (mistake.Counter !== 0) {
            const isMistake = 1;
            return res.render('reserve', { fields, isMistake });
        }
    } catch (err) {
        res.write('Error while checking reservation', err);
        res.status(500);
        res.end();
    }

    return next();
}
