// Setting the min and max date if it was not set by the user
export default function checkFilterReservations(req, res, next) {
    const from = new Date(req.body.minDate);
    const till = new Date(req.body.maxDate);

    if (!req.body.minDate || !(from.getTime() === from.getTime() + 1 - 1)) {
        req.body.minDate = '1974-01-01T00:00';
    }

    if (!req.body.maxDate || !(till.getTime() === till.getTime() + 1 - 1)) {
        req.body.maxDate = '2050-01-01T00:00';
    }

    return next();
}
