// Check if uploaded file is photo
export default function checkPhotoUpload(req, res, next) {
    const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
    const fileHandler = req.files.photoForSportField;
    if (!validImageTypes.includes(fileHandler.type)) {
        return res.status(400).send('The file is not a picture');
    }

    return next();
}
